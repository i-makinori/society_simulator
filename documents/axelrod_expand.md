# 拡張されたアクセルロッドの文化変容モデル

- http://mas.kke.co.jp/modules/tinyd4/index.php?id=15
セルキュアオートマトンの一種
文化として解釈される点に於いて､その文化を示すパラメータとしての文字が交換され､次の文化と成る｡

- 若しくは､AFD現象｡
集合し融合し分化発展する(Aggregation Fusion Differentiation)


# 妄想した新概念

### 文化レイヤー

文化が集まった文化もまた文化であり､
文化の一部もまた文化なのである｡
若しくは､文化と言いつつも､細胞膜の本質に迫るものかも知れない｡

村八分や寄生虫などが見られたら面白そうである｡

拡大率ごとの等高線の表示､等高線がその密度でのその文化パラメータの密度でもある｡ 地図DBへのアクセスと同様のアルゴリズム｡

1. 地形: 電磁気､日照時間､気温､水､陸､気温､標高､陸地､資源､植物､動物(3のレイヤー単位である地球それ自身でもある)
2. 最小意識単位｡ヒト｡ 文字をもち､状態により解釈し､文字を変換する｡ 
3. 分化(≒文化)単位｡ 3の最小レーヤはAFD現象の集積された最小単位であり､意思決定は L2.x ヒト(ソマチッド)である｡ 
...
...
n. 発見である文字の集積､文字である遺伝子の集積､彼らも知らずとて参照する文字の集積､プログラムである文字の集積｡
n+1. その世界の総てがここに記録される｡

- 注釈
  - L3ではその瞬間である時間範囲の中での個が観測される｡
  - L4には個と個の相互作用が表示される機能がある｡
  - L5では操作手によるカオスな分岐が設定される｡
  - n はどの値ともたり得る｡

### 文化の認識

母親が等しい範囲を辿った範囲を文化の範囲というのか､
等しい言語･構文の範囲を文化の範囲というのか､
同じ文字､即ちプログラムの関連付けの範囲を文化というのか､
単に近い距離に居るだけで文化というのか､
距離とは物理的な距離なのか､想いの距離なのか｡

### 意味次元による可視化
意味が軸となる｡
4d実存世界でのxyzt｡
Voidの同集合体範囲を示す等高線､クラスタ
思念次元でのフィルターされたグラフ構造｡
思念のクラスタの網｡



### 地形

移動にコストがかかること､生態系が異なること｡

現実世界での動植物の生態系もまた文化であり､動植物それ自身もまた文化なのである｡



### 移動
- エージェントは移動する
文化を為すエージェントもまた文化であり､
エージェントの文化もまたエージェントなのである｡

### 行動による文化の獲得
- 動詞､感情､距離 => 色や文字や一部の履歴などとして表示される｡

静的な系では､即ち､内部の恒常的なパターンに従う場合､
動詞の入力に対する､そのエージェントは一度結論したパラメータである文化に対しては､同じ結論がなされる｡ 
これは､読み込むプログラムに対しての機構,即ち解釈系が､関数の適用範囲内では変位せず､同じパターンの出力がなされるためである｡

動的な系では､機構としてのエージェントの論理回路を為すもの変位した場合､環境が異なった場合(即ち同じに見える全く異なる動詞が入力された場合)
例えば､エージェントが論理的な解を得ようとした場合や､著名人が死んだ時､扇動されている時､発明･発見が為されようとしている時､文化パラメータに対する解釈が変異した場合には､
そのネットワークかも知れない回路が異なる結論をなし､新たな文化のパターンである文字を獲得し､その時のプログラムに紐づけされる｡

文化の集積した文化は､マクロを生成しうる｡


### 代数

本来は､文字の演算構造として表現されるが､当節では興味の範囲外につき数値により代用する｡

- 文化の距離
```
Dist = nDSqrt(sum(|C1.n-C2.n|))
```





